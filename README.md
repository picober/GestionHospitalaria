<h1>Gestion Hospitalaria usando GNU Health</h1> 
<p><strong>Autor:</strong> Miguel Arturo Bernechea</p>
<h2>Resumen</h2>
<h3>Descripción</h3>
<p>GNU Health es un proyecto de Software Libre para profesionales de la salud, instituciones de salud y gobiernos, es una adaptación del sistema de gestión de empresas/organizaciones (ERP) denominado tryton, el cual esta conformado por un modelo de tres capas, el servidor, el cliente y la base de datos.</p>
<p>Su diseño modular permite que se implemente en muchos diferentes escenarios: desde pequeñas oficinas privadas, hasta grandes sistemas nacionales de salud pública. Cubre la funcionalidad de Historia Clínica Electrónica (HCE), Gestión Hospitalaria y Sistema de Información en Salud (SIS).</p>
<h3>Objetivos del Proyecto</h3>
<p>El proyecto consiste en la puesta en marcha de GNU health usando un sistema operativo debian 9, base de datos en postgresql, python 3, cliente/servidor tryton, cliente web, y todos los paquetes y dependencias necesarias que el sistema requiera para su correcto funcionamiento.</p>
<p>El objetivo principal del proyecto es el funcionamiento de un servidor que aloje el sistema hospitalario, de manera que las diferentes áreas puedan conectarse y realizar las operaciones correspondientes a su sector.</p>
<p>El servidor deberá realizar las gestiones referidas a: </p>
<ul>
	<li>Gestion del Paciente</li>
	<li>Enfemería</li>
	<li>Compras y stock</li>
	<li>Administración</li>
	<li>Historia Clínica</li>
	<li>Farmacia</li>
	<li>Ambulancia</li>
	<li>Laboratorios</li>
</ul>
<h2>Estado de Arte</h2>
<p>El proyecto Gnu health a nivel internacional está implementado en varias instituciones clínicas y hospitalarias, un proyecto a principio orientado y pensado para zonas de escasos recursos, que logro repercutir mundialmente hasta ser premiado como el mejor softwarte libre ( año 2012).</p>
<p>Algunas implementaciones:</p>
<ul>
	<li>MEDICAL CENTER PVT. LTD., CHITTAGONG – BANGLADESH</li>
	<li>VICTORIA HOSPITAL, ZAMBIA</li>
	<li>THE ACADEMIC HOSPITAL PARAMARIBO (AZP), SURINAME</li>
	<li>PEERLESS HOSPITAL, NIGERIA</li>
	<li>MOJOWARNO CHRISTIAN HOSPITAL, INDONESIA</li>
	<li>GRIYA HUSADA HOSPITAL, INDONESIA</li>
	<li>GRIYA WALUYA HOSPITAL, INDONESIA</li>
</ul>
<p>A nivel Nacional:</p>
<ul>
<li>SAN VICENTE PRIMARY CARE CENTER, ARGENTINA</li>
<li>DR OÑATIVIA REGIONAL CENTER, ARGENTINA</li>
<li>
	<p>ALPI, BUENOS AIRES – ARGENTINA : ALPI,  es una asociación civil sin fines de lucro que se dedica a la rehabilitación neuromotriz de pacientes pediátricos y adultos.</p>
	<p>Una vez más, pionero en Argentina.  Esta vez a través de la implementación del Software Libre por el sistema de Gestión Hospitalaria y de Información de Salud (GNU Health), el cual procura proporcionar e-salud y educación para países de economías emergentes.</p>
	<p>A través de GNU Health la institución cambia la forma actual de administrar las historias clínicas por un procedimiento digitalizado. Cada médico especialista, así como  otros sectores de asistencia, podrán visualizar en forma automática las historias clínicas de los pacientes del Centro de Internación y de Atención Ambulatoria. Este procedimiento posibilitará un ahorro en tiempo de búsqueda de información, además de un orden mayor al guardar la documentación de cada paciente digitalmente.</p>
</li>
<li>
	<p>Hospital LISTER, (Entre RÍos, Argentina): El hospital Joseph Líster es el primer hospital público de la provincia de Entre Ríos en informatizar las historias clínicas de sus pacientes empleando el sistema de gestión hospitalaria GNU Health.</p>
	<p>Objetivo: presentar las adecuaciones del sistema para adaptarlo a las necesidades del hospital y evolución de los registro sanitarios a los 32 meses de la implementación.</p>
	<p>La implementación del mismo se llevó a cabo durante los meses de Septiembre y Diciembre de 2012 en tres etapas de una semana de duración cada una.</p>
	<p>En la primera etapa se instaló y acondicionó toda la infraestructura necesaria junto al software necesario para la implementación (Sistemas Operativos y GNU Health). Se comenzó a utilizar el software utilizando los módulos de manejo de citas, pacientes, laboratorio, radiología, enfermería e historia clínica.</p>
	<p>En la segunda etapa se incorporaron al sistema e manejo de farmacia, prescripción de medicamentos e internaciones.</p>
	<p>Por último, en la tercera etapa se conjugaron los datos de las primeras dos para incorporar así el manejo de la estadística de la institución. Cada una de las etapas de implementación fue acompañada de las correspondientes capacitaciones.</p>
	<p>Se realizaron las migraciones de 15 equipos a GNU/Linux.</p>
	<ul>
		<li>1745 pacientes regustrados aprox.</li>
		<li>2267 evaluaciones.</li>
		<li>1199 servicios de ambulancia</li>
		<li>82 hospitalizaciones</li>
		<li>9684 gestiones de stock</li>
	</ul>
</li>
</ul>
<p>A nivel Provincial:</p>
<p>No se encontró información alguna referida a la implementación del sistema a nivel provincial (Jujuy - Argentina), ni en ninguna de sus localidades del interior.</p>
<h2>Objetivos Generales</h2>
<p>En este trabajo se presenta las adecuaciones del sistema GNU Health en el proceso de adaptación a las necesidades del hospital Belgrano de la localidad de Humahuaca (Provincia de Jujuy).</p>
<p>El sistema público de salud de la localidad de Humahuaca cuenta con ??? para atender a una población de 20.000 personas aprox. Los trabajos realizados actualmente estan basados en cuadernos y el manejo de insumos en formularios y hojas de cálculo para el control de entrada y salida de productos.
</p>
<p>Lo que se desea lograr es la informatización de los sectores importantes como administración, de manera de tener nucleado bajo un único servidor todas las prestaciones para que los clientes usuarios puedan acceder de acuerdo a su perfil o rol dentro del hospital (administración, enfermería, pacientes, stock, etc)</p>
<h2>Objetivos Especificos</h2>
<h3>Materiales y Métodos</h3>
<p>Se relevaron los procesos de atención de pacientes y administrativos del hospital. En diálogo con la encargada administrativa donde dió a conocer las necesidades fundamentales a informatizar. La metodología de trabajo usada actualmente esta basada en el uso de planillas de cálculo como una de las formas de registrar y procesar los datos. Se informó al personal sobre el ensayo de implementación del sistema con sus respectivas pruebas y errores.</p>
<p>Los sectores a informatizar son: administración, control de stock, gestión de pacientes, historia clínica.</p>
<p>Cáda sector se corresponde con un módulo prediseñado del sistema GNU Health para instalación y uso, a medida que el personal avance en lo que refiere a capacitación de uso se irán implementando el resto de los sectores.</p>
<h3>Beneficios a señalar</h3>
<ul>
	<li>Social: La implementación de un sistema hospitalario abierto beneficiará a la institución permitiendo el manejo de los datos sensibles de manera rápida y eficiente, reduciendo demoras en la atención de los pacientes y el público en general.</li>
	<li>Económico: La aplicación del software GNU Health puede considerarse una alternativa sólida, confiable y económica, no requiere costos monetarios para su instalación y actualización.</li>
	<li>Técnico: Instalación y documentación técnica disponible para el sector que lo requiera.</li>
</ul>
<h3>Cronograma de Trabajo</h3>
<p>Dividido en 3 fases de una semana de duración cada una.</p>
<table align="center" border="1" width="95%" cellpadding="3" cellspacing="0" rules="all">
	<tr align="center" bgcolor="#d9d9d9"><th>Semana</th><th>Actividades</th><th>Observación</th></tr>
	<tr><td rowspan="5" align="center" bgcolor="#d9d9d9"><b>1</b></td><td>- Relevamiento Situación Actual</td>
		<td></td>
	</tr>
	<tr><td>- Equipos informáticos Usados</td>
		<td></td>
	</tr>	
	<tr><td>- Software actual utilizado y Estado de Red</td>
		<td></td>
	</tr>	
	<tr><td>- Problemas a resolver</td>
		<td></td>
	</tr>	
	<tr><td>- Administración de Riesgos</td>
		<td></td>
	</tr>
	<tr><td rowspan="5" align="center" bgcolor="#d9d9d9"><b>2</b></td><td>- Recursos Hardware Requeridos</td>
		<td></td>
	</tr>
	<tr><td>- Instalación de Sistema Operativo Base</td>
		<td></td>
	</tr>	
	<tr><td>- Instalación de paquetes necesarios</td>
		<td></td>
	</tr>	
	<tr><td>- Instalación del Núcleo del Servidor GNU Health</td>
		<td></td>
	</tr>	
	<tr><td>- Puesta en Marcha y solución de errores</td>
		<td></td>
	</tr>	
	<tr><td rowspan="5" align="center" bgcolor="#d9d9d9"><b>3</b></td><td>- Instalación de Módulos específicos para cada sector</td>
		<td></td>
	</tr>
	<tr><td>- Prueba de Acceso mediante navegador Web</td>
		<td></td>
	</tr>	
	<tr><td>- GNU Health integración con dispositivos móbiles</td>
		<td></td>
	</tr>	
	<tr><td>- Gestión de Documentación y Tutoriales de Uso</td>
		<td></td>
	</tr>	
	<tr><td>- Capacitación al Personal</td>
		<td></td>
	</tr>	
</table>
<h2>Conclusiones</h2>
<p>De los encuentros con el personal encargado de la administración, se llega a la conclusión que la implementación del software, una vez iniciado, estará en fase de prueba/error durante un período de 3 meses para luego calcular costos destinados a la adquisición y renovación de equipamientos, routers, cableado, computadoras portátiles y de escritorio. Una vez terminada la fase de prueba se prentende realizar diálogos a nivel político para obtener asesoramiento y conformar un área específica de Informática dedicada al mantenimiento del servidor GNU Health. </p>
